<?php

/**
 * Created by PhpStorm.
 * User: RedHat
 * Date: 15.07.2017
 * Time: 20:45
 */
function detDataAlter($url) {
    $keywords = array();
    $domain = array($url);
    $doc = new DOMDocument;
    $doc->preserveWhiteSpace = FALSE;
    foreach ($domain as $key => $value) {
        @$doc->loadHTMLFile($value);
        $anchor_tags = $doc->getElementsByTagName('a');
        foreach ($anchor_tags as $tag) {
            $keywords[] = strtolower($tag->nodeValue);
        }
    }
}

function get_data($url) {
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
    $returned_content = $data;
    echo $returned_content;
    $error = substr($returned_content, 2, 5);
    echo $error;
}

function downloadDataCoin($url) {

    $returned_content = get_data($url);

    $regexp = "<tr>(.*)target=\"_blank\">(.*)<\/a>(.*)price(.*)data-btc=\"(.*)\"(.*)<\/tr>";
    preg_match_all("/$regexp/siU", $returned_content, $matches);

    for($i = 0; $i < sizeof($matches[2]); $i++)
        $result[$matches[2][$i]] = $matches[5][$i];
    return $result;
}

function showTableWithResult($firstMarket, $secondMarket) {

    for($i = 1; $i < sizeof($firstMarket); $i++) {
        $key = array_keys($firstMarket)[$i];
        if(array_key_exists($key, $firstMarket) && array_key_exists($key, $secondMarket)){
            @$sort[$key] = number_format((($secondMarket[$key] - $firstMarket[$key]) / $secondMarket[$key]) * 100,2,'.',',');
            @$result[$key] = "<td>".$key."</td><td>".$firstMarket[$key]."</td><td>".$secondMarket[$key]."</td><td>". ($secondMarket[$key] - $firstMarket[$key]) ."</td><td>".$sort[$key]."%</td>";
        }
    }
    arsort($sort);
    echo "first - second <BR><table>
";
    foreach ($sort as $key => $value) {
        echo "<tr>" . $result[$key] . "</tr>";
    }
    echo "</table>";
}

/*
 * IMPORTANT DATA TO FILL
 * Link must be from coinmarketcap and must contains main site market
 */

$URLFirstMarketOnCoinMarketCap  = "https://coinmarketcap.com/exchanges/bittrex/#BTC";
$URLSecondMarketOnCoinMarketCap = "https://coinmarketcap.com/exchanges/yobit/#BTC";

$firstMarket    = downloadDataCoin($URLFirstMarketOnCoinMarketCap);
$secondMarket   = downloadDataCoin($URLSecondMarketOnCoinMarketCap);

showTableWithResult($firstMarket, $secondMarket);
